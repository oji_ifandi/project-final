@extends('layout.master')

@section('judul')
Halaman List Pertanyaan
@endsection

@section('content')

<div class="row">
@forelse ($pertanyaan as $item)
    <div class="col-12">
        <div class="card">
          <!-- <h5 class="card-header">Featured</h5> -->
          <div class="card-body">
            <h1>{{$item->judul}}</h1>
            <span class="badge badge-primary">{{$item->kategori->nama}}</span>
            <p class="card-text">{{$item->content}}</p>
            <a href="/pertanyaan/{{$item->id}}" class="btn btn-primary">Detail</a>
          </div>
        </div>
    </div>
    @empty
        <tr colspan="3">
            <td>Tidak ada data</td>
        </tr>  
    @endforelse
</div>



@endsection