@extends('layout.master')

@section('judul')
Halaman Detail Pertanyaan
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
          <!-- <h5 class="card-header">Featured</h5> -->
          <img src="{{asset('images/'.$pertanyaan->file)}}" alt="">
          <div class="card-body">
            <h5 class="card-title">{{$pertanyaan->judul}}</h5>
            <p class="card-text">{{$pertanyaan->content}}</p>
            <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method('delete')
                <!-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-primary">Detail</a> -->
                <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info">Edit</a>
                <input type="submit" value="Hapus" class="btn btn-danger">
            </form>

          </div>
        </div>
    </div>
</div>

<div>
    <h3>Jawaban</h3>
    @forelse ($pertanyaan->jawaban as $item)
    <div class="col-12">
        <div class="card">
          <h5 class="card-header">{{$item->user->name}}</h5>
          <div class="card-body">
            <h1>{{$item->judul}}</h1>
            <p class="card-text">{{$item->content}}</p>
          </div>
        </div>
    </div>
    @empty
        <tr colspan="3">
            <td>Belum ada jawaban</td>
        </tr>  
    @endforelse
    <hr>
    
    <form action="/jawaban" method="post">
      @csrf
      <input type="hidden" value="{{$pertanyaan->id}}" name="pertanyaan_id">
      <textarea name="content" class="form-control"></textarea><br>
      <input type="submit" class="btn btn-success" value="Jawaban">
    </form>
</div>

@endsection