@extends('layout.master')

@section('judul')
Halaman Detail Kategori
@endsection

@section('content')


<div class="row">
    @forelse ($kategori->pertanyaan as $item)
    <div class="col-12">
        <div class="card">
          <!-- <h5 class="card-header">Featured</h5> -->
          <img src="{{asset('images/'.$item->file)}}" alt="">
          <div class="card-body">
            <h5 class="card-title">{{$item->judul}}</h5>
            <p class="card-text">{{$item->content}}</p>
            <form action="/item/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <!-- <a href="/item/{{$item->id}}" class="btn btn-primary">Detail</a> -->
                <a href="/item/{{$item->id}}/edit" class="btn btn-info">Edit</a>
                <input type="submit" value="Hapus" class="btn btn-danger">
            </form>

          </div>
        </div>
    </div>
    @empty
        <tr colspan="3">
            <td><h1>Tidak ada data</h1></td>
        </tr>  
    @endforelse
</div> 

@endsection