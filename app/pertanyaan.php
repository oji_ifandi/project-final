<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class pertanyaan extends Model
{
    protected $table = 'pertanyaan';

    protected $fillable = ['judul', 'content', 'file', 'kategori_id', 'user_id'];
    

    public function kategori()
    {
        return $this->belongsTo('App\kategori', 'kategori_id' );
    }

    public function jawaban()
    {
        return $this->hasMany('App\jawaban');
    }
}
