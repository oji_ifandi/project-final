<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = 'kategori';

    protected $fillable = ['nama', 'deskripsi'];

    public function pertanyaan()
    {
        return $this->hasMany('App\pertanyaan');
    }
}
